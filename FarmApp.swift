//
//  FarmApp.swift of project named
//
//  Created by Aurore D (@Tuwleep) on 29/03/2023.
//
//

import Foundation

//=====================
// PARAMETERS
//=====================

var money = 0.0

// price for feed animals
var feedAnimalsPrice = 4.00

// Products
struct Product {
    var name: String
    var unit: String
    var price: Double
}

var milk = Product(name: "lait", unit: "litres", price: 0.50)
var wheat = Product(name: "blé", unit: "kilos", price: 0.30)
var wool = Product(name: "laine", unit: "kilos", price: 1.00)

// set up barn
var barn: [String: Double] = ["milk": 0.0, "wheat": 0.0, "wool": 0.0]

//=====================
// FUNCTION MENU
//=====================

// For Start
while true {
    startMenu()
}

/// don't give the right answer
func dontUnderstand() {
    print("Je n'ai pas compris votre réponse")
}

/// back to start menu
func backStart() {
    startMenu()
}

//=====================
// FUNCTION CONSULT
//=====================

/// displays the amount of money
func consultMoney() {
    print("vos économies sont de \(money) euros !")
}

/// displays content of barn
func consultBarn() {
//    guard let barnMilk = barn["milk"] else {
//        return
//    }
        
        print("""
            
            Votre grange contient :
            🥛  \(barn["milk"] ?? 0.0) \(milk.unit) de \(milk.name)
            🌾  \(barn["wheat"] ?? 0.0) \(wheat.unit) de \(wheat.name)
            🐑  \(barn["wool"] ?? 0.0) \(wool.unit) de \(wool.name)
            
            """)
}

//=====================
// FUNCTION BARN
//=====================

/// Ask confirm before sell and reset the barn
/// Can confirm or back in start menu
func askConfirmForSell() {
    print("""
    
    Avez-vous vendu les produits que contenait votre grange ?
    1. Confirmer
    x. Retour au début
    """)
    if let confirm = readLine() {
        switch confirm {
        case "1":
            sellAndReset()
        case "x":
            backStart()
        default:
            dontUnderstand()
        }
    }
}

// reset barn
func resetBarn() {
    barn = ["milk": 0.0, "wheat": 0.0, "wool": 0.0]
}
/// use after askConfirmForSell() for sell and reset barn
/// `Action:`
///  - sell all product
///  - add amount in money
///  - reset barn
///  - displays sales details
func sellAndReset() {
    let sellMilk = Double(barn["milk"]!) * milk.price
    let sellWheat = Double(barn["wheat"]!) * wheat.price
    let sellWool = Double(barn["wool"]!) * wool.price
    
    let total = sellMilk + sellWheat + sellWool
    
    money += total
    resetBarn()
    
    print("""
    Vos marchandises ont été vendues et \(total) euros ont été ajoutés à vos économies, soit :
        🥛  \(sellMilk) euros de \(milk.unit) de \(milk.name)
        🌾  \(sellWheat) euros de \(wheat.unit) de \(wheat.name)
        🐑  \(sellWool) euros de \(wool.unit) de \(wool.name)

""")
}

//=====================
// FUNCTION ACTIVITY
//=====================

/// substract and display the price to feed the animals
func feedAnimals() {
    money -= feedAnimalsPrice
    print("Votre activité à été prise en compte et \(feedAnimalsPrice) euros ont été retirés de vos économies.")
}

/// Calculate quantity of product to add
/// - Parameters:
///  - of name : product.name,
///  - in type : product.unit
/// - Returns: quantity
func howMany(of name: String,in type: String) -> Double? {
    print("Combien de \(type) de \(name) voulez vous ajouter ?")
    if let answer = readLine() {
        if let quantity = Double(answer){
            print(quantity)
            return quantity
        } else {
            print("Ben non")
            return nil
        }
    }
    print("et donc ?")
    return nil
}

///add milk in barn and print it
///use howmany() for have quantity
func milkCows() {
    if let quantity = howMany(of: milk.name, in: milk.unit){
            barn["milk"] = Double(barn["milk"]!) + quantity
            print("Vous avez ajouté \(quantity) \(milk.unit) de \(milk.name) à votre grange.")
    } else {
            print("Erreur lors de l'enregistrement")
    }
}

///add wheat in barn and print it
///use howmany() for have quantity
func harvest() {
    if let quantity = howMany(of: wheat.name, in: wheat.unit){
        barn["wheat"] = Double(barn["wheat"]!) + quantity
        print("Vous avez ajouté \(quantity) \(wheat.unit) de \(wheat.name) à votre grange.")
    } else {
        print("Erreur lors de l'enregistrement")
    }
}

///add wool in barn and print it
///use howmany() for have quantity
    func mowSheep() {
        if let quantity = howMany(of: wool.name, in: wool.unit){
            barn["wool"] = Double(barn["wool"]!)  + quantity
            print("Vous avez ajouté \(quantity) \(wool.unit) de \(wool.name) à votre grange.")
        } else {
            print("Erreur lors de l'enregistrement")
        }
    }

//=====================
// DISPLAY MENU
//=====================

func startMenu(){
    print("""
    
    *** MENU PRINCIPAL **
    
    Que voulez vous faire ?
    1. 🚜 Enregistrer une nouvelle activité
    2. 💰 Consulter mes économies
    3. 📦 Consulter ma grange
    """)
    if let choice = readLine(){
        switch choice {
        case "1":
            addActivity()
        case "2":
            consultMoney()
        case "3":
            consultBarn()
        default:
            dontUnderstand()
        }
    }
    
    
    
}

func addActivity() {
    print("""
            
        *** ENREGISTRER UNE NOUVELLE ACTIVITÉ ***
        
        Qu'avez vous fait aujourd'hui ?
        1. 🐮 J'ai nourri mes animaux
        2. 🥛 J'ai trait mes vaches
        3. 🌾 J'ai moissonné
        4. 🐑 J'ai tondu mes moutons
        5. 💰 J'ai vendu mes produits
        x. Retour au début
        """)
    if let choice = readLine() {
        switch choice {
        case "1":
            feedAnimals()
        case "2":
            milkCows()
        case "3":
            harvest()
        case "4":
            mowSheep()
        case "5":
            askConfirmForSell()
        case "x":
            backStart()
        default:
            dontUnderstand()
        }
    }
}
