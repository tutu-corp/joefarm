# FarmApp
FarmWithUI is an application to calculate how much a farmer can save.
## General info
This project is based on an OpenClassroom tutorial.
The training was to used the terminal.

## Screenshot
|Menu|Display Money and Barn|Activity
|--|--|--|
|![Menu](./captures/menu.png)|![Menu](./captures/consult.png)|![Menu](./captures/newActivity.png)|