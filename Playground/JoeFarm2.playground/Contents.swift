// Le prix du voyage
let price = 1499.0

// L'argent de Joe
var money = 0.0

// Le nbr de jours pendant lesquels Joe doit économiser
var numberOfDay = 0

// La grange
var barn = ["milk": 0, "wheat": 0, "wool": 0]
var feedCow = 4.00
var milkQuantity = 30
var milkPrice = 0.50
var wheatQuantity = 100
var wheatPrice = 0.30
var woolQuantity = 30
var woolPrice = 1.00

//calcul de la taille de la grange
func calculateBarnSize() -> Int {
    var barnSize = 0
    for (_, count) in barn {
        barnSize += count
    }
    return barnSize
}

// nourrir les vaches
func feedCows() {
    money -= feedCow
}

while money < price {
    feedCows()
    if let milk = barn["milk"], let wheat = barn["wheat"], let wool = barn["wool"] {
        
        // tondre les moutons
        func mowSheep() {
            barn["wool"] = wool + woolQuantity
        }
        
        // Traire les vaches
        func milkCows() {
            barn["milk"] = milk + milkQuantity
        }
        
        // Moissonner
        func harvest() {
            barn["wheat"] = wheat + wheatQuantity
        }
        
        // Tout vendre et reset de la grange
        func sellAndReset() {
            money = money + Double(milk) * milkPrice
            money = money + Double(wheat) * wheatPrice
            money = money + Double(wool) * woolPrice
            barn = ["milk": 0, "wheat": 0, "wool": 0]
        }
        	
        if calculateBarnSize() >= 500 {
            sellAndReset()
        } else {
            // C'est une journée normale
            if numberOfDay % 30 == 1 {
                harvest()
            } else if numberOfDay % 30 == 10 || numberOfDay % 30 == 20 {
                mowSheep()
            } else {
                milkCows()
            }
        }
        
        // On passe au jour suivant
        numberOfDay += 1
    }
}
print("Il aura fallu \(numberOfDay) jours à Joe pour économiser \(money) €")



