import UIKit

var greeting = "Hello, playground"

let price: Double = 1499.0
var money = 0.0
var numberOfDay : Int = 0

//     Modulo permet d'avoir le reste suite à un division d'entier, dans le cas d'une semaine sur une base de 0 à 6 pour les jours de la semaine (Lundi = 0, Dimanche = 6), le fait d'avoir le modulo, par le diviseur 7, permet d'avoir le jour précis.
//    0 % 7 = 0 = Lundi
//    7 % 7 = 0 = Lundi
//    3 % 7 = 3 = Jeudi
//    26 % 7 = 5 = Samedi
//    Pareil pour un nombre de jours dans le mois si divisé par les 30 jours du mois le modulo permet d'identifier a quel jour nous en sommes 3 % 30 = 3 nous sommes le 3 du mois



26 % 4
26 % 7
3 % 30

var milkPrice = 0.50
var milkQuantity = 30.0

var whoolPrice = 1.00
var whoolQuantity = 30.0

var wheatPrice = 0.30
var wheatQuantity = 100.0


var day = 0

while money < price {
    // eat for cow
    money -= 4
    // day + 1
    numberOfDay += 1
    // first day = wheat
    if numberOfDay % 30 == 1 {
        money += wheatQuantity * wheatPrice
        // Sheep 10 and 20 of month
    } else if numberOfDay % 30 == 10 || numberOfDay % 30 == 20 {
        money += whoolQuantity * whoolPrice
    } else {
        // sell milk
        money += milkQuantity * milkPrice
    }
}

print("il m'a fallut \(numberOfDay) jours pour gagner \(money) € ")

// Milk, Wheat, Whool
// var barn : [Int] = [0, 0, 0]

// déclaration des dictionnaires vides
var emptyDict: [String: Int] = [:]
var emptyDict2 = [String: Int]()

// Création et modification d'un dictionnaire
var countries : [String:String] = ["FR": "France", "IT": "Italie", "ES": "Espagne"]
print("J'ai \(countries.count) pays dans ma liste")
countries["UK"] = "United Kingdom"
print("J'ai \(countries.count) pays dans ma liste :")
for (code, country) in countries {
    print("Le pays \(country) a pour code \(code)")
}

// retrait d'une valeur
countries.removeValue(forKey: "ES")
print("J'ai \(countries.count) pays dans ma liste :")
for (code, country) in countries {
    print("Le pays \(country) a pour code \(code)")
}

// Création de la grange de Joe en dictionary
var barn: [String:Int] = ["Milk":0, "Wheat":0, "Whool":0]


var dict = [true: 1, false: -1]

var optional: Int? = 12
var entier: Int
// Renvoi nil car déclaré mais sans valeur

// Variable en ? est de type optional même si défini en tant que int/String/float/double..
// si optional = type optional = Je suis un optional et je contient peut être un Int/String/Float/Double

// permet de renvoyer le contenu de l'optinal en autre type que l'optional dans un var ou un let
if let variableDéballé = optional{
    print(variableDéballé)
}

func reponseUniverselle() -> Int? {
    42
}

func afficherReponseUniverselle() -> String {
    // vérifie si l'optional contient quelque chose, si vide ne rentre pas dans le if,
//    la variable nom est accessible uniquement dans le cadre du if let
    if let nom = reponseUniverselle() {
        print(nom)
        return "la réponse est \(nom)"
    }
        return "nil"
//    return "la réponse est \(nom)"      renvoi une erreur car nom n'est pas utilisable ici
}
print(afficherReponseUniverselle())

func afficherReponseUniverselle2() -> String {
//    vérifie si optional est vide, si vide rentre dans le if,
//    la variable nom est accessible en dehors du guard let
    guard let nom = reponseUniverselle() else {
        return "nil"
//    return "la réponse est \(nom)"      renvoi une erreur car nom n'est pas utilisable ici
    }
    print(nom)
    return "la réponse est \(nom)"
}
print(afficherReponseUniverselle2())

// Avec est l'étiquette de la fonction, permet meilleur lisibilité, pour ne pas mettre d'étiquette remplacer par _ soit func catchMovie(_ nom: String){}
func catchMovie(avec nom: String){
    print("Je regarde un film avec " + nom)
}

catchMovie(avec: "Luc")
